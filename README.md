<!-- Editor: Tuyet M. Phan -->

# Getting with docpress

## Introduction

```
docpress is a tools for documentation during work
```

## Install

```
npm install -g docpress
```

## Run docpress 

```
docpress serve
```

## Structure Organization

```md
docs
|-README.md
|-file.md
```

- Note:
    - docs: folder
    - file.md: Content need to show
    - README.md: content TOC of the document

## Note with README.md

- Need start with a single reference after wanting to group them.

- For example: in README.md

``` md

Should:

# Name of document

- [link](file.md)

Shouldn't:

# Name of document

- Part 1
    - [link](file.md)

```

## In case: You want to convert rst-to-markdown

Example with ubuntu

- Install pandoc

``` 
sudo apt-get update && sudo apt-get install pandoc   

```

 At the folder content *.rst that you want to convert to markdown:

- Add this file (rst-to-md.sh)

```sh
#!/usr/bin/env bash
 
FILES="*.rst"
for f in $FILES
do
  filename="${f%.*}"
  echo "Converting '$f' to '$filename.md'"
  pandoc "$f" -f rst -t markdown -o "$filename.md"
done

```
-  Allow excute:

```
chmod u+x ./rst-to-md.sh
```

- Start converting:

```
./rst-to-md.sh

```

- Note: pandoc is just a basic tool to convert rst to mardown, besides you need to learn html to convert rst in case pandoc cant do that.
